"use strict";
const express = require('express'); //insere express para tratar as requisições
const DB = require('./db'); //módulo para lidar com o bando de dados
const config = require('./config');
const bcrypt = require('bcrypt'); //ferramenta para encriptação da senha
const jwt = require('jsonwebtoken'); //ferramenta para gerar token de acesso do usuário
const bodyParser = require('body-parser'); //módulo para ler o corpo da requisição

const db = new DB("sqlitedb") //inicializa o banco de dados como sqlite3 
const app = express(); //inicializa express
const router = express.Router(); //inicializa router do express


//configurações do bodyparser
router.use(bodyParser.urlencoded({ extended: false })); 
router.use(bodyParser.json()); 

// CORS middleware
const allowCrossDomain = function(req, res, next) { 
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
}

//Used in order to use the API
app.use(allowCrossDomain)

//TODO NOT LOGIN AFTER REGISTRATION

router.post('/register', function(req, res) { //rota para registrar um novo usuário
    //método para inserir um novo usuário
    db.insert([  //Insere um novo usuário
        req.body.name, //insere o atributo 'name' do corpo da requisição
        req.body.email, //insere o atributo 'email' do corpo da requisição
        bcrypt.hashSync(req.body.password, 8) //insere o hash da senha do corpo da requisição, 8 é o fator de custo do hash
    ],
    function (err) { //função para tratar o pós requisição
        if (err) return res.status(500).send("There was a problem registering the user.") //Se existir algum erro dá erro numero 500 e manda uma mensagem

        db.selectByEmail(req.body.email, (err,user) => {   //Tenta acessar o usuário cadastrado
            if (err) return res.status(500).send("There was a problem getting user")  //retorna um erro caso tenha algum problema no processo
            let token = jwt.sign({ id: user.id }, config.secret, {expiresIn: 300 // configura o token
            });
            res.status(200).send({ auth: true, token: token, user: user }); //Caso tudo esteja certo envia a confirmação com o token, usuário, e autenticçaão
        });
    });
});

//TODO NOT LOGIN AFTER REGISTRATION


//rota para registrar um usuário admnistrador
router.post('/register-admin', function(req, res) { //declaração da rota
    db.insertAdmin([ //insere um administrador no bando de dados
        req.body.name, //insere o nome
        req.body.email, //insere o seu email
        bcrypt.hashSync(req.body.password, 8), //insere o hash da sua senha
        1
    ],
    function (err) { //função para tratar o pós requisição
        if (err) return res.status(500).send("There was a problem registering the user.") //envia erro caso haja um erro
        db.selectByEmail(req.body.email, (err,user) => { 
            if (err) return res.status(500).send("There was a problem getting user")
            let token = jwt.sign({ id: user.id }, config.secret, { expiresIn: 300 // configura o token de acesso expires in 24 hours expiresin está em ms
            });
            res.status(200).send({ auth: true, token: token, user: user }); //envia a resposta com o token, usuário e confirmação de autenticação
        });
    });
});

router.post('/login', (req, res) => { //rota para realizar login no sistema 
    db.selectByEmail(req.body.email, (err, user) => { //seleciona por email dentro do DB com o email que está no corpo da requisição
        if (err) return res.status(500).send('Error on the server.'); //caso haja algum erro ele retorna status 500 e uma mensagem de erro no servidor
        if (!user) return res.status(404).send('No user found.'); //caso usuário não existe envia erro 404 e a mensagem de usuário não encontraro
        let passwordIsValid = bcrypt.compareSync(req.body.password, user.user_pass); //verifica se a senha é válida em um processo de comparação 
        if (!passwordIsValid) return res.status(401).send({ auth: false, token: null }); //se a senha não for válida ele retorna um erro 401, e nega a autenticação
        let token = jwt.sign({ id: user.id }, config.secret, { expiresIn: 300 // caso tudo dê certo, ele valida a autorização
        });
        res.status(200).send({ auth: true, token: token, user: user });
    });
})

app.use(router)

let port = process.env.PORT || 3000;

let server = app.listen(port, function() {
    console.log('Servidor Desafio SESI de Pesquisa na porta  ' + port)
});



