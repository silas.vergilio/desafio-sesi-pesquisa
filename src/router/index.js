import Vue from 'vue' //importa vue
import Router from 'vue-router' //importa o vue-router
import HelloWorld from '@/components/HelloWorld' //importa o componente Hello World
import Login from '@/components/Login' //importa o componente Login
import Register from '@/components/Register' //importante componente Register
import UserBoard from '@/components/UserBoard' //importa o componente da dashboard do usuário
import Admin from '@/components/Admin' //importa compoenente do painel do adminstrador

Vue.use(Router) //Faz com que Vue use o Router

let router = new Router({ //Definição do objeto do router com rotas e requisitos
  mode: 'history', //definição do modo do router que usa o mais comum com '/rota'e sem #
  routes: [ //declaração das rotas
    {
        path: '/', //Caminho padrão da rota, ou 'home page'
        name: 'HelloWorld', //nome da rota
        component: HelloWorld //componente que seja utilizado na rota
    },
    {
        path: '/login', //Caminho da rota login
        name: 'login', //nome da rota login
        component: Login, //Componente utilizado
        meta: {
            guest: true //rota liberado para convidados, ou seja, não precisa de login
        }
    },
    {
        path: '/register',    //caminho para registro
        name: 'register', //nome da rota
        component: Register,//Componente que será chamado
        meta: {
            guest: true //Rota liberado sem acesso por login 
        }
    },
    {
        path: '/dashboard', //caminho da dashboard do sistema
        name: 'userboard', //nome da rota da dashboard
        component: UserBoard, //componente da dashboard que será chamado com os dados do usuário
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/admin', //rota de página específica para administrador 
        name: 'admin', //nome da rota
        component: Admin, //componente de adminsitrador
        meta: {
            requiresAuth: true, //requer autenticação
            is_admin : true //requer que a autenticação seja
        }
    },
]
})

router.beforeEach((to, from, next) => { //função que vai controlar tudo que acontece antes de ir a uma rota
  if(to.matched.some(record => record.meta.requiresAuth)) { //verifica se a rota a qual o sistema está direcionado necessidade de autenticação
      if (localStorage.getItem('jwt') == null) { //verifica se existe um token valido ou não , caso sej anull ele não está logado
          next({ //
              path: '/login', //ele então redireciona para a página de login
              params: { nextUrl: to.fullPath }
          })
      } else { //caso contrário, ou seja, o usuário tenha a autenticação
          let user = JSON.parse(localStorage.getItem('user')) //ele primeiramente pega o usuário do localstore
          if(to.matched.some(record => record.meta.is_admin)) { //verifica se a rota necessita que o usuário seja adminsitrador
              if(user.is_admin == 1){ //verifica se o usuário é administrador
                  next() //prossegue com a rota
              }
              else{
                  next({ name: 'userboard'}) //caso contrário manda ele para uma página que um usuário não administrador tem acesso
              }
          }else {
              next()
          }
      }
  } else if(to.matched.some(record => record.meta.guest)) { //verifica se a rota é destinada a usuários não logados
      if(localStorage.getItem('jwt') == null){ //verifica se há ou não um token associado no localStorage
          next() //segue com o fluxo da rota
      }
      else{
          next({ name: 'userboard'}) //caso contrário, vai para a página do usuário
      }
  }else {
      next() //caso não seja esse tipo de rota, segue o fluxo dela
  }
})

export default router


